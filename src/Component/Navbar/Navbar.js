import React from 'react'
import HomeLogo from '../../Image/HomeLogo.png'
import './Navbar.scss'
export default function Navbar() {
  return (
    <div className="navbar">
      <img src={HomeLogo} alt=""/>
      <div className="subnav">
          <p>Platform</p>
          <p>Brands</p>
          <p>Tools</p>
          <label >Login</label>
      </div>
    </div>
  )
}
